# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Code Assignement for Gina Mako (3rd of November 2020)

* The purpose of the many commits is to give an insight to the code reviewer into my working steps

* I provided a bit more functionality than required by the specification in order to deliver a better User Experience (UX is a very important aspect of the software in my opinion). 
This piece of software should give the reviewer an understanding of 
the quality of my work as well as my work ethics and a hint of the output that can be expected from me.

* As for the unit tests, I provided some for the validations service. More Unit Tests / Selenium Tests can be added upon request.

* The specification that was provided by the customer gives an idea of the roadmap and how the application might grow in the near future. 
I prepared the buidlings-page with a possible future use of ngrx in my head. 
My goal was to prepare the application in a way that a future upscaling by using ngrx/state-management would be easily donw without a major refactoring of the whole application.

* I invested a bit of time in the scss files to demonstrate the usage of mixins and placeholders and clean css coding as well as encapsulation of styles in components when it made sense. 
There is a lot of potential of inheriting from styleguide classes/colors/mixins that are usually provided by other internal libraries (such as styleguide/branding etc). 
An up-scaling of the sass files would make more sense when the company's library stack becomes clear.

* The responsiveness of the apps and browser compatibility was not in the main focus as this falls back to the styleguide topic. To be done later.

* As it was not required, I didn't setup a multi-language solution. This would have been one of my first questions to the requirement engineer though.

More ***questions*** that I would have had for the requirement engineers - before setting up the application as the answers would have had a major impact on the software architecture:
 
    * how many languages do we expect?
    * is there a cms system in use where the texts come from?
    * how many users will work with this applications?
    * what is the traffic that we expect?
    * is there a concept of user permissions? who is allowed to change what data?
    * how does the user log in?
    * do you want to show also images of the buildings? is there a multimedia database that provides this images?
    * please paint me a diagram where the data comes from and where the data goes?
    * who is the target group for this application?
    * are we consuming any (public) APIs? (e.g. postal data for validations if zipCode and City are a valid match)
    * what other data, besides building and their rooms do you want to manage with this application? 
    
    
### How do I get set up? ###

```
git clone https://ginamako@bitbucket.org/ginamako/swissre-work-assignement-10.git
```

```
npm i
```

```
npm run start
```

Open http://localhost:4200 in the browser of your choice


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
