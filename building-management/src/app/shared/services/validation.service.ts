import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {AbstractControl, AsyncValidatorFn, ValidationErrors} from '@angular/forms';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() {
  }

  nicknameValidator(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      return this.isValidNickname(control.value).pipe(
        map(res => {
          return !res ? {nicknameNotUnique: true} : null;
        })
      );
    };
  }

  // TODO Gina: as soon as the backend service is ready, finish the implementation of the validator
  isValidNickname(nickname: string): Observable<boolean> {
    const result = !nickname.trim().toLowerCase().startsWith('a');
    return of(result).pipe();
  }

}
