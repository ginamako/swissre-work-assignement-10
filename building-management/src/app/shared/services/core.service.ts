import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CoreService {

  constructor() {
  }

  /**
   * Returns a deep copy of the object
   *
   * I would use cloneDeep from lodash for this type of actions
   *    but the requirement was not to use any additional libraries other than rxjs
   */
  public deepCopy(oldObj: any): any {
    let newObj = oldObj;
    if (oldObj && typeof oldObj === 'object') {
      if (oldObj instanceof Date) {
        return new Date(oldObj.getTime());
      }
      newObj = Object.prototype.toString.call(oldObj) === '[object Array]' ? [] : {};
      // tslint:disable-next-line:forin
      for (const i in oldObj) {
        newObj[i] = this.deepCopy(oldObj[i]);
      }
    }
    return newObj;
  }

  public pushAt(arr: any[], index, newItem: any): any {
   arr.splice(index, 0, newItem);
   return arr;
  }

  public pushAtNextToLast(arr: any[], newItem: any): any {
    const index = arr.length - 1;
    return this.pushAt(arr, index, newItem);

  }

}
