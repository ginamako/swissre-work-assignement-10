import {fakeAsync, TestBed} from '@angular/core/testing';

import {ValidationService} from './validation.service';
import {FormControl} from '@angular/forms';

describe('ValidationService', () => {
  let service: ValidationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ValidationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return false if name starts with "a"', fakeAsync(() => {
    const obs = service.isValidNickname('almighty');
    obs.subscribe(result => {
      expect(result).toBeFalse();
    });
  }));

  it('should return false if name starts with capital "A" ', fakeAsync(() => {
    const obs = service.isValidNickname('Almighty');
    obs.subscribe(result => {
      expect(result).toBeFalse();
    });
  }));

  it('should return false if name starts with any other letter than "a" ', fakeAsync(() => {
    const obs = service.isValidNickname('Big Ben');
    obs.subscribe(result => {
      expect(result).toBeTrue();
    });
  }));

  it('should test the validator with control value starting with "a"  ', fakeAsync(() => {
    const control = new FormControl(
      'almighty',
      {
        asyncValidators: [service.nicknameValidator()]
      }
    );

    expect(control.invalid).toBeTrue();
  }));

  it('should test the validator with control value starting with "A"  ', fakeAsync(() => {
    const control = new FormControl(
      'Almighty',
      {
        asyncValidators: [service.nicknameValidator()]
      }
    );

    expect(control.invalid).toBeTrue();
  }));

  it('should test the validator with control value starting with "B"  ', fakeAsync(() => {
    const control = new FormControl(
      'Blue Moon',
      {
        asyncValidators: [service.nicknameValidator()]
      }
    );

    expect(control.invalid).toBeFalse();
  }));

});
