import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavigationComponent} from './components/navigation/navigation.component';
import {AddressComponent} from './components/address/address.component';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NicknamesEditorComponent} from './components/nicknames/nicknames-editor.component';
import {MatInputModule} from '@angular/material/input';
import {OrderByPipe} from './pipes/order-by.pipe';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import { ValidationIndicatorComponent } from './components/validation-indicator/validation-indicator.component';

// all material modules that are needed everywhere are being loaded here: MatIconModule, MatButtonModule etc
// the rest is being loaded on demand by the modules that need them

@NgModule({
  declarations: [
    NavigationComponent,
    AddressComponent,
    NicknamesEditorComponent,
    ValidationIndicatorComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule,
    MatInputModule,
    MatTableModule,
    MatFormFieldModule
  ],
  exports: [
    NavigationComponent,
    AddressComponent,
    NicknamesEditorComponent,
    FormsModule,
    ReactiveFormsModule,
    // material modules
    MatIconModule,
    MatButtonModule,
    MatTooltipModule,
    MatInputModule,
    MatTableModule,
    MatFormFieldModule,
  ],
  providers: [
    OrderByPipe
  ]
})
export class SharedModule {
}
