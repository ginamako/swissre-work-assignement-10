import {BuildingDto} from '../dtos/BuildingDto';

export const buildingsMock: BuildingDto[] = [
  {
    buildingId: 1,
    buildingName: 'The London Gherkin',
    description: 'The building stands on the former site of the Baltic Exchange (24-28 St Mary Axe), which was the headquarters of a global marketplace for shipping freight contracts and also soft commodities, and the Chamber of Shipping (30-32 St Mary Axe).',
    address: {
      addressId: 1,
      street: 'St Mary Axe',
      houseNumber: '30',
      city: 'London',
      zipCode: '10171',
      countryCode: 'UK'
    },
    nicknames: [
      {
        nicknameId: 4711,
        sortingOrder: 10,
        nickname: 'The Egg'
      },
      {
        nicknameId: 4712,
        sortingOrder: 20,
        nickname: 'The Gherkin'
      },
      {
        nicknameId: 4713,
        sortingOrder: 40,
        nickname: 'Swiss Re Tower'
      },
      {
        nicknameId: 4714,
        sortingOrder: 30,
        nickname: 'Swiss Re Building'
      }
    ]
  },
  {
    buildingId: 2,
    buildingName: 'Chrysler Building',
    description: 'The Chrysler Building is an Art Deco–style skyscraper located in the Turtle Bay neighborhood on the East Side of Manhattan, New York City, at the intersection of 42nd Street and Lexington Avenue near Midtown Manhattan',
    address: {
      addressId: 2,
      street: 'Oxford Plaza',
      houseNumber: '5280',
      city: 'New York',
      zipCode: '9760-406',
      countryCode: 'US'
    }
  },
  {
    buildingId: 3,
    buildingName: 'Primetower',
    description: 'The Prime Tower, also named "Maag-Tower" in an earlier stage of planning, is a skyscraper in Zurich, Switzerland. At a height of 126 metres, it was the highest skyscraper in Switzerland from 2011 until 2015, when the Roche Tower in Basel was finished. The building is located near the Hardbrücke railway station.',
    address: {
      addressId: 3,
      street: 'Hardbrücke',
      houseNumber: '15',
      city: 'Zürich',
      zipCode: '8005',
      countryCode: 'CH'
    }
  },
];
