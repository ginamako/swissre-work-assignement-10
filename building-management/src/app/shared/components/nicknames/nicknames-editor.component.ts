import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NicknameDto} from '../../dtos/NicknameDto';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatTableDataSource} from '@angular/material/table';
import {CoreService} from '../../services/core.service';
import {ValidationService} from '../../services/validation.service';

@Component({
  selector: 'app-nicknames-editor',
  templateUrl: './nicknames-editor.component.html',
  styleUrls: ['./nicknames-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NicknamesEditorComponent implements OnInit {
  NEW_NICKNAME_ID = 0;
  NICKNAME_LENGTH = 50;

  @Output() saveNicknames = new EventEmitter<NicknameDto[]>();

  dynamicForm: FormGroup = new FormGroup({});
  dataSource: MatTableDataSource<NicknameDto>;
  displayedColumns: string[];
  amountOfNicknames = 0;
  displayInternalFields: boolean;
  // until the change event is triggered by the save button
  nicknamesLocal: NicknameDto[];

  // keeping a local copy of the nickname in order not to change the original data

  constructor(
    private formBuilder: FormBuilder,
    private coreService: CoreService,
    private el: ElementRef,
    private validationService: ValidationService) {
  }

  @Input() set nicknames(value: NicknameDto[]) {
    // deep cloning the nicknames in order not to change the original data on the fly
    // the data in the Building Object will be changed on submit of the whole nicknames form, given the form is in a valid state

    // add a spare nickname with id=0 in case the user wants to immediately create a new nickname
    // the field for the potential new nickname will be called nickname_0
    let newValue = value ? [...this.coreService.deepCopy([...value])] : [];
    newValue = [...newValue, this.getNewNicknameObject()];

    this.nicknamesLocal = newValue;
    this.amountOfNicknames = newValue?.length || 0;
    this.registerControls();
  }

  ngOnInit(): void {
    this.displayedColumns = this.displayInternalFields
      ? ['nicknameId', 'nickname', 'sortingOrder', 'action']
      : ['nickname', 'action'];
  }

  registerControls(): void {
    // recreate form
    if (this.nicknamesLocal) {
      this.dynamicForm = new FormGroup({});
      this.nicknamesLocal.forEach(item => {
        // generating controls with dynamic name with the pattern nickname_i.
        // using the prefix "nickname_" in case we decide to make the sortingOrder field as an input too
        //    then we can use for that as a name pattern sortingOrder_i
        this.dynamicForm.addControl(
          this.getNicknameFieldName(item.nicknameId),
          new FormControl(
            item.nickname,
            {
              validators: item.nicknameId !== this.NEW_NICKNAME_ID ? [Validators.required] : [],
              asyncValidators: [this.validationService.nicknameValidator()],
              // only check on blur instead on key press to have a better performance
              updateOn: 'blur'
            }
          )
        );
      });
    }

    // recreate table data
    this.dataSource = new MatTableDataSource(this.nicknamesLocal);

    // set focus on the new field
    this.setFocusToNewNicknameField();

    // subscribe to changes
    this.onFormValueChanges();
  }

  onFormValueChanges(): void {
    // listen for changes on the new nickname (which should be triggered on blur)
    this.dynamicForm.get(this.getNicknameFieldName(this.NEW_NICKNAME_ID)).valueChanges.subscribe(newNickname => {
      this.addNewNickname(newNickname);
    });
  }

  onSubmit(): void {
    // stop here if form is invalid
    if (this.dynamicForm.invalid) {
      return;
    }

    // remove the pseudo last item (empty nickname) before saving
    const newNicknames = this.coreService.deepCopy(this.nicknamesLocal).splice(0, this.nicknamesLocal.length - 1);
    this.saveNicknames.emit(newNicknames);
  }

  removeNickname(n: NicknameDto): void {
    // first of all, remove the nickname from the array
    const index: number = this.nicknamesLocal.findIndex((i) => i.nicknameId === n.nicknameId);
    if (index !== -1) {
      this.nicknamesLocal.splice(index, 1);
    }

    // 2) regenerate the form and the table to refresh the UI correctly
    this.registerControls();
  }

  getNicknameField(id: number): AbstractControl {
    return this.dynamicForm.controls[this.getNicknameFieldName(id)];
  }

  getNicknameFieldName(id: number): string {
    return 'nickname_' + id;
  }

  getNewNicknameObject(): NicknameDto {
    return {
      nicknameId: this.NEW_NICKNAME_ID,
      nickname: ''
    } as NicknameDto;
  }

  getPlaceholderForNicknameField(id: number): string {
    return id === this.NEW_NICKNAME_ID ? 'New nickname' : 'Nickname';
  }

  addNewNickname(nickname: string): void {
    const newNickname = this.getNewNicknameObject();
    // set the id of the nickname to the next one based on the existing data
    newNickname.nicknameId = Math.max.apply(Math, this.nicknamesLocal.map((o) => {
      return o.nicknameId;
    })) + 1;
    newNickname.sortingOrder = Math.max.apply(Math, this.nicknamesLocal.map((o) => {
      return o.sortingOrder;
    })) + 10;
    newNickname.nickname = nickname;

    this.nicknamesLocal = this.coreService.pushAtNextToLast(this.nicknamesLocal, newNickname);

    // regenerate the form and the table to refresh the UI correctly
    this.registerControls();
  }

  updateNickname(item: NicknameDto, event): void {
    if (item.nicknameId !== this.NEW_NICKNAME_ID) {
      // update only existing items ad the new item is being handled differently in onFormValueChanges
      const result = this.nicknamesLocal.find(n => n.nicknameId === item.nicknameId);
      if (result) {
        result.nickname = event.target.value?.trim();
      }
    }
  }

  getNewNicknameFormField(): AbstractControl {
    return this.dynamicForm.get(this.getNicknameFieldName(this.NEW_NICKNAME_ID));
  }

  setFocusToNewNicknameField(): void {
    // set the focus to the newly generated field so that the user can type the new nickname right away
    const element = this.el.nativeElement
      .querySelector(`[id="${this.getNicknameFieldName(this.NEW_NICKNAME_ID)}"]`);
    if (element) {
      element.focus();
    }
  }

  resetForm(): void {
    console.log('not implemented due to missing specification');
  }
}
