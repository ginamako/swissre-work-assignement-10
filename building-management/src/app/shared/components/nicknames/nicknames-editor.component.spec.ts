import {ComponentFixture, TestBed} from '@angular/core/testing';

import {NicknamesEditorComponent} from './nicknames-editor.component';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

describe('NicknamesEditorComponent', () => {
  let component: NicknamesEditorComponent;
  let fixture: ComponentFixture<NicknamesEditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NicknamesEditorComponent],
      imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule
      ],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NicknamesEditorComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
