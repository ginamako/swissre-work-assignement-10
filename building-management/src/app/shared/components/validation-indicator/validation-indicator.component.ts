import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'app-validation-indicator',
  templateUrl: './validation-indicator.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ValidationIndicatorComponent {
  @Input() tooltipText: string;
  @Input() isVisible: boolean;
  @Input() text: string;
}
