import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {AddressDto} from '../../dtos/AddressDto';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddressComponent{
  @Input() address: AddressDto;
  @Input() renderFlat: boolean;
}
