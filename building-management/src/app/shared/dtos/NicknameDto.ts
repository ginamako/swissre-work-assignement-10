export interface NicknameDto {
  nicknameId: number;
  nickname: string;
  sortingOrder: number;
}

