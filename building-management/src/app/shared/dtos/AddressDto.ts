export interface AddressDto {
  addressId: number;
  street: string;
  houseNumber: string;
  zipCode: string;
  city: string;
  countryCode: string;
}

