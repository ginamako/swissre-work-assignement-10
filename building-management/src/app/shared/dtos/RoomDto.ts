import {NicknameDto} from './NicknameDto';

export interface RoomDto {
  roomId: number;
  roomNumber: string;
  description?: string;
  nicknames?: NicknameDto[];
}

