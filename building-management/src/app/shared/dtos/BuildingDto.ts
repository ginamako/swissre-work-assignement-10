import {AddressDto} from './AddressDto';
import {NicknameDto} from './NicknameDto';
import {RoomDto} from './RoomDto';

export interface BuildingDto {
  buildingId: number;
  buildingName: string;
  description: string;
  address: AddressDto;
  nicknames?: NicknameDto[];
  rooms?: RoomDto[];
}

