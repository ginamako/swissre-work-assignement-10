import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BuildingsPageComponent} from './buildings-page.component';
import {BuildingViewComponent} from './components/building-view/building-view.component';
import {BuildingsListComponent} from './components/buildings-list/buildings-list.component';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {SharedModule} from '../shared/shared.module';
import {MatCardModule} from '@angular/material/card';


@NgModule({
  declarations: [BuildingsPageComponent, BuildingViewComponent, BuildingsListComponent],
  imports: [
    CommonModule,
    SharedModule,
    MatTableModule,
    MatFormFieldModule,
    MatCardModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class BuildingsModule {
}
