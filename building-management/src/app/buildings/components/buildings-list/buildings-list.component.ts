import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {BuildingDto} from '../../../shared/dtos/BuildingDto';

@Component({
  selector: 'app-buildings-list',
  templateUrl: './buildings-list.component.html',
  styleUrls: ['./buildings-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BuildingsListComponent {
  @Output() editBuildingNicknames = new EventEmitter<BuildingDto>();
  displayedColumns: string[] = ['buildingId', 'buildingName', 'address', 'nicknames', 'action'];
  dataSource: MatTableDataSource<BuildingDto>;

  @Input() selectedBuilding: BuildingDto;

  @Input() set buildings(value: BuildingDto[]) {
    this.dataSource = new MatTableDataSource(value);
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
