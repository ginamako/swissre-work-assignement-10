import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {BuildingDto} from '../../../shared/dtos/BuildingDto';
import {NicknameDto} from '../../../shared/dtos/NicknameDto';

@Component({
  selector: 'app-building-view',
  templateUrl: './building-view.component.html',
  styleUrls: ['./building-view.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BuildingViewComponent {
  @Input() selectedBuilding: BuildingDto;
  @Output() saveNicknames = new EventEmitter<NicknameDto[]>();
}
