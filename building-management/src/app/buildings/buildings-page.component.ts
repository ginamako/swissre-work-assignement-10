import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {BuildingDto} from '../shared/dtos/BuildingDto';
import {BuildingsService} from './services/buildings.service';
import {NicknameDto} from '../shared/dtos/NicknameDto';


@Component({
  selector: 'app-buildings',
  templateUrl: './buildings-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BuildingsPageComponent {
  constructor(private buildingsService: BuildingsService,
              private cd: ChangeDetectorRef) {
  }

  get buildings(): BuildingDto[] {
    return this.buildingsService.buildings;
  }

  get selectedBuilding(): BuildingDto {
    return this.buildingsService.selectedBuilding;
  }

  editBuildingNicknames(building: BuildingDto): void {
    if (building) {
      this.buildingsService.selectedBuilding = building;
    }
  }

  saveNicknames(nicknames: NicknameDto[]): void {
    const building = this.selectedBuilding;
    building.nicknames = nicknames;
    this.buildingsService.updateSelectedBuilding(building);
    this.cd.markForCheck();

    // log to console on success as specified
    console.log('SUCCESS!! :-)\n\n');
  }
}
