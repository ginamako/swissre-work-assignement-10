import {Injectable} from '@angular/core';
import {BuildingDto} from '../../shared/dtos/BuildingDto';
import {buildingsMock} from '../../shared/mocks/buildings.mock';

@Injectable({
  providedIn: 'root'
})
export class BuildingsService {
  selectedBuilding: BuildingDto;

  // TODO Gina: as soon as the backend service is ready, load the data correctly from the backend
  buildings = buildingsMock;

  constructor() {
    this.selectedBuilding = this.buildings ? this.buildings[0] : null;
  }

  updateSelectedBuilding(item: BuildingDto): void {
    this.buildings.find(i => i.buildingId === item.buildingId)[0] = item;
    this.selectedBuilding = item;
  }
}
