import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BuildingsPageComponent} from './buildings/buildings-page.component';

const routes: Routes = [
  {path: '', redirectTo: '/buildings', pathMatch: 'full'},
  {path: 'buildings', component: BuildingsPageComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
